"use strict";
var BasicLine = /** @class */ (function () {
    function BasicLine(prevEnd) {
        var _this = this;
        this.drawingCallback = function (event) {
            console.log(event);
            var length = _this.curves.length;
            if (length == 2) {
                _this.parentElement.removeEventListener("mousemove", _this.mouseMoveCallback);
                _this.line.setAttribute("stroke", "black");
                _this.parentElement.removeEventListener("click", _this.drawingCallback);
                _this.button.disabled = false;
                _this.lineDrawn = true;
            }
        };
        this.mouseMoveCallback = function (event) {
            var validCoords = _this.findAngle([event.clientX, event.clientY], _this.prevEnd ? _this.prevEnd : [600, 300]);
            _this.line.setAttribute("x2", "" + validCoords[0]);
            _this.line.setAttribute("y2", "" + validCoords[1]);
            if (_this.curves.length == 1) {
                _this.curves.push([validCoords[0], validCoords[1]]);
            }
            else {
                _this.curves[0][0] = validCoords[0];
                _this.curves[0][1] = validCoords[1];
                lastEndCoords = validCoords;
            }
        };
        this.parentElement = document.querySelector(".parent");
        this.curves = [];
        this.line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
        this.lineDrawn = false;
        this.button = document.querySelector(".createLine");
        this.parentElement.append(this.line);
        this.firstClickMarkup = null;
        this.coordAxis = null;
        this.basicCoords = [300, 300];
        this.prevEnd = prevEnd;
        // first line
        var basicLine = document.createElementNS('http://www.w3.org/2000/svg', 'line');
        basicLine.setAttribute('x1', '300');
        basicLine.setAttribute('y1', '300');
        basicLine.setAttribute('x2', '600');
        basicLine.setAttribute('y2', '300');
        basicLine.setAttribute("stroke", "black");
        this.parentElement.append(basicLine);
    }
    BasicLine.prototype.startDrawing = function () {
        this.parentElement.addEventListener("click", this.drawingCallback);
        this.button.disabled = true;
        var startPoint = [this.prevEnd ? this.prevEnd[0] : 600, this.prevEnd ? this.prevEnd[1] : 300];
        this.line.setAttribute("x1", "" + startPoint[0]);
        this.line.setAttribute("y1", "" + startPoint[1]);
        this.curves.push(startPoint);
        this.line.setAttribute("stroke", "black");
        this.parentElement.addEventListener("mousemove", this.mouseMoveCallback);
    };
    BasicLine.prototype.findAngle = function (coords, startingPoint) {
        if (startingPoint === void 0) { startingPoint = [600, 300]; }
        var height = Math.abs(coords[1] - startingPoint[1]);
        var width = Math.abs(coords[0] - startingPoint[1]);
        var lineLength = Math.sqrt(Math.pow(startingPoint[0] - coords[0], 2) + Math.pow(startingPoint[1] - coords[1], 2));
        var sin = height / lineLength;
        var pi = Math.PI;
        var angle = Math.asin(sin) * 180 / pi;
        var roundAngle = Math.round(angle / 90 * 4) * 22.5;
        if (roundAngle > 67.5) {
            return [startingPoint[0], coords[1]];
        }
        if (roundAngle == 0) {
            return [coords[0], startingPoint[1]];
        }
        return [coords[0], coords[1] < startingPoint[1] ? startingPoint[1] - Math.abs(startingPoint[0] - coords[0]) * Math.tan(roundAngle * Math.PI / 180) : startingPoint[1] + Math.abs(startingPoint[0] - coords[0]) * Math.tan(roundAngle * Math.PI / 180)];
    };
    return BasicLine;
}());
var lastEndCoords = null;
document.querySelector(".createLine").addEventListener("click", function () {
    var line = new BasicLine(lastEndCoords);
    line.startDrawing();
});
