type prevEnd = number[] | null;

class BasicLine {
	parentElement: SVGElement;
	curves: Array<number[]>;
	line: SVGLineElement;
	lineDrawn: boolean
	button: HTMLButtonElement
	firstClickMarkup: SVGCircleElement | null;
	basicCoords: number[];
	coordAxis: "NW" | "NE" | "SW" | "SE" | null;
	prevEnd: prevEnd
	constructor(prevEnd: prevEnd) {
		this.parentElement = document.querySelector(".parent")!;
		this.curves = [];
		this.line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
		this.lineDrawn = false;
		this.button = document.querySelector(".createLine")!;
		this.parentElement.append(this.line);
		this.firstClickMarkup = null;
		this.coordAxis = null;
		this.basicCoords = [300, 300];
		this.prevEnd = prevEnd;

		// first line
		const basicLine = document.createElementNS('http://www.w3.org/2000/svg', 'line');
		basicLine.setAttribute('x1', '300');
		basicLine.setAttribute('y1', '300');
		basicLine.setAttribute('x2', '600');
		basicLine.setAttribute('y2', '300');
		basicLine.setAttribute("stroke", "black");
		this.parentElement.append(basicLine);
	}
	public startDrawing() {
		this.parentElement.addEventListener("click", this.drawingCallback);
		this.button.disabled = true;
		const startPoint: number[] = [this.prevEnd ? this.prevEnd[0] : 600, this.prevEnd ? this.prevEnd[1] : 300];
		this.line.setAttribute(`x1`, `${startPoint[0]}`);
		this.line.setAttribute(`y1`, `${startPoint[1]}`);
		this.curves.push(startPoint);
		this.line.setAttribute("stroke", "black");
		this.parentElement.addEventListener("mousemove", this.mouseMoveCallback);
	}

	private drawingCallback = (event: MouseEvent) => {
		console.log(event);
		const length = this.curves.length;
		if (length == 2) {
			this.parentElement.removeEventListener("mousemove", this.mouseMoveCallback);
			this.line.setAttribute("stroke", "black");
			this.parentElement.removeEventListener("click", this.drawingCallback);
			this.button.disabled = false;
			this.lineDrawn = true;
		}
	}

	private mouseMoveCallback = (event: MouseEvent) => {
		const validCoords = this.findAngle([event.clientX, event.clientY], this.prevEnd ? this.prevEnd : [600, 300])
		this.line.setAttribute(`x2`, `${validCoords[0]}`);
		this.line.setAttribute(`y2`, `${validCoords[1]}`);
		if (this.curves.length == 1) {
			this.curves.push([validCoords[0], validCoords[1]]);
		}
		else {
			this.curves[0][0] = validCoords[0];
			this.curves[0][1] = validCoords[1];
			lastEndCoords = validCoords;
		}
	}

	private findAngle(coords: number[], startingPoint: number[] = [600, 300]) {
		const height = Math.abs(coords[1] - startingPoint[1]);
		const width = Math.abs(coords[0] - startingPoint[1]);
		const lineLength = Math.sqrt(Math.pow(startingPoint[0] - coords[0], 2) + Math.pow(startingPoint[1] - coords[1], 2));
		const sin = height / lineLength;
		const pi = Math.PI;
		const angle = Math.asin(sin) * 180 / pi;
		const roundAngle = Math.round(angle / 90 * 4) * 22.5;
		if (roundAngle > 67.5) {
			return [startingPoint[0], coords[1]]
		}
		if (roundAngle == 0) {
			return [coords[0], startingPoint[1]]
		}
		return [coords[0], coords[1] < startingPoint[1] ? startingPoint[1] - Math.abs(startingPoint[0] - coords[0]) * Math.tan(roundAngle * Math.PI / 180) : startingPoint[1] + Math.abs(startingPoint[0] - coords[0]) * Math.tan(roundAngle * Math.PI / 180)]
	}


}

let lastEndCoords: number[] | null = null
document.querySelector(".createLine")!.addEventListener("click", () => {
	const line = new BasicLine(lastEndCoords);
	line.startDrawing()
})